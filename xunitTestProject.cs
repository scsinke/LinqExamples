using Xunit;

namespace LinqExamples
{
  [CollectionDefinition(nameof(NotThreadSafeResourceCollection), DisableParallelization = true)]
  public class NotThreadSafeResourceCollection { }
}
