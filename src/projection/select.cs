using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;

// Opdracht 3 projection (select, selectmany)
namespace LinqExamples.projection
{
  public class SelectDemo
  {
    public static IEnumerable<List<SchoolClass>> Select(IEnumerable<Student> students)
    {
      // HIGHLIGHT query expression
      return
        from student in students
        select student.Classes;
    }

    public static IEnumerable<SchoolClass> SelectMany(IEnumerable<Student> students)
    {
      // HIGHLIGHT method based query
      return students.SelectMany(st => st.Classes);
    }
  }
}
