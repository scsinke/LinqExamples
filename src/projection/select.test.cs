using System.Collections.Generic;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.projection
{
  public class SelectTest
  {
    [Theory]
    [MemberData(nameof(GetStudents))]
    public void LinqSelectTest(List<Student> students)
    {
      var res = SelectDemo.Select(students);

      foreach (var classesList in res)
      {
        foreach (var studentClass in classesList)
        {
          Assert.True(studentClass.Name == "WAPP" || studentClass.Name == "WIN");
        }
      }
    }

    [Theory]
    [MemberData(nameof(GetStudents))]
    public void SelectManyTest(List<Student> students)
    {
      var res = SelectDemo.SelectMany(students);

      foreach (var studentClass in res)
      {
        Assert.True(studentClass.Name == "WAPP" || studentClass.Name == "WIN");
      }
    }


    public static IEnumerable<object[]> GetStudents()
    {
      yield return new object[]
      {
        new List<Student>() {
          new Student("Carsten", "Sinke", 4,
            new List<SchoolClass>(){
              new SchoolClass("WAPP", new List<int>(){8,6,10}),
              new SchoolClass("WIN", new List<int>(){4,6,5}),
            }),
          new Student("Elon", "Tusk", 4,
            new List<SchoolClass>(){
              new SchoolClass("WAPP", new List<int>(){4,6,5}),
              new SchoolClass("WIN", new List<int>(){1,7,6}),
            }),
          new Student("Daniele", "Dicardo", 3,
            new List<SchoolClass>(){
              new SchoolClass("WAPP", new List<int>(){1,7,6}),
              new SchoolClass("WIN", new List<int>(){8,6,10}),
            }),
        }
      };
    }
  }
}
