using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;

namespace LinqExamples.grouping
{
  // Opdracht 5 Grouping (group by)
  class Group
  {
    public static IEnumerable<ReturnTradeGrouped> GroupByRange(IEnumerable<Trade> trades)
    {
      var groupingRange = new[] { 100, 1000, 10000 };

      return trades
        .GroupBy(t => groupingRange.First(c => t.Price <= c))
        .Select(g =>
          new ReturnTradeGrouped { Range = g.Key, Trades = g.Select(r => r) }
        );
    }
  }

  class ReturnTradeGrouped
  {
    public int Range { get; set; }
    public IEnumerable<Trade> Trades { get; set; }
  }
}