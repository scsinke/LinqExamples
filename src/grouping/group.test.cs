using System.Collections.Generic;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.grouping
{
  public class GroupTest
  {

    [Theory]
    [MemberData(nameof(GetTrades))]
    public void GroupingTest(List<Trade> trades)
    {
      // Act
      var groupedTrades = Group.GroupByRange(trades);

      // Assert
      foreach (var tradeRange in groupedTrades)
      {
        foreach (var trade in tradeRange.Trades)
        {
          Assert.True(trade.Price <= tradeRange.Range);
        }
      }
    }

    public static IEnumerable<object[]> GetTrades()
    {
      yield return new object[]
      {
        new List<Trade>() {
          new Trade("TSLA", 366.28),
          new Trade("GOOGL", 1547.23),
          new Trade("AMZN", 3268.61),
          new Trade("RDSA", 12.03),
          new Trade("AIR", 69.58),
          new Trade("FB", 273.72),
          new Trade("MSFT", 211.29),
          new Trade("AAPL", 177.32),
          new Trade("NKE", 144.90),
          new Trade("DIS", 133.36),
          new Trade("NVDA", 508.60),
        }
      };
    }
  }
}
