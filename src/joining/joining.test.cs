using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.joining
{
  public class JoiningTest
  {

    [Theory]
    [MemberData(nameof(GetCompaniesAndCities))]
    public void InnerJoinTest(List<Company> companies, List<City> cities)
    {
      var firstResult = Joining.InnerJoin(companies, cities).First();

      Assert.Equal("Villa Optica", firstResult.CompanyName);
      Assert.Equal("Apeldoorn", firstResult.City);
    }

    [Theory]
    [MemberData(nameof(GetCompaniesAndCities))]
    public void FullOuterJoinTest(List<Company> companies, List<City> cities)
    {
      // Arrange
      var expected = new List<ReturnObject>()
      {
        new ReturnObject(){CompanyName = "Villa Optica", City = "Apeldoorn"},
        new ReturnObject(){CompanyName = "Sandmann Optics", City = "Apeldoorn"},
        new ReturnObject(){CompanyName = "4D Optics", City = "Apeldoorn"},
        new ReturnObject(){CompanyName = "Pearl", City = "Duiven"},
        new ReturnObject(){CompanyName = "Hans Anders", City = "Arnhem"},
        new ReturnObject(){CompanyName = "Specsavers", City =""},
        new ReturnObject(){CompanyName = "", City = "Amsterdam"},
      };
      // Act
      var res = Joining.FullOuterJoin(
        companies,
        cities
      ).ToList();

      // Assert
      for (int i = 0; i < expected.Count; i++)
      {
        Assert.Equal(expected[i].City, res[i].City);
      }
    }

    public static IEnumerable<object[]> GetCompaniesAndCities()
    {
      yield return new object[]
      {
        new List<Company>() {
          new Company("Villa Optica", 1),
          new Company("Sandmann Optics", 1),
          new Company("4D Optics", 1),
          new Company("Pearl", 2),
          new Company("Hans Anders", 3),
          new Company("Specsavers", 5),
        },
        new List<City>() {
          new City("Apeldoorn", 1),
          new City("Duiven", 2),
          new City("Arnhem", 3),
          new City("Amsterdam", 4)
        }
      };
    }
  }
}
