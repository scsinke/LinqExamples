using System.Collections.Generic;
using System.Linq;
using LinqExamples.linqExtensions;
using LinqExamples.models;

namespace LinqExamples.joining
{
  // Opdracht 4 Joining (join, outer join)
  class Joining
  {
    public static IEnumerable<ReturnObject> InnerJoin(IEnumerable<Company> companies, IEnumerable<City> cities)
    {
      return
        from company in companies
        join city in cities on company.CityId equals city.Id
        select new ReturnObject { CompanyName = company.Name, City = city.Name };
    }

    public static IEnumerable<ReturnObject> FullOuterJoin(IEnumerable<Company> companies, IEnumerable<City> cities)
    {
      var leftOuterJoin =
        from company in companies
        join city in cities on company.CityId equals city.Id into gj
        from supCity in gj.DefaultIfEmpty()
        select new ReturnObject
        {
          CompanyName = company.Name,
          City = supCity?.Name ?? string.Empty
        };

      var rightOuterJoin =
        from city in cities
        join company in companies on city.Id equals company.CityId into gj
        from subCompany in gj.DefaultIfEmpty()
        select new ReturnObject
        {
          CompanyName = subCompany?.Name ?? string.Empty,
          City = city.Name
        };

      return leftOuterJoin.Union(rightOuterJoin).DistinctBy(r => new { r.CompanyName, r.City });
    }
  }

  class ReturnObject
  {
    public string CompanyName { get; set; }
    public string City { get; set; }
  }
}