using System;
using System.Collections.Generic;
using Xunit;

namespace LinqExamples.misc
{
  public class MiscellaneousTest
  {
    [Fact]
    public void SkipTest()
    {
      // Arrange
      var numbers = new List<int>() { 1, 2, 3, 4, 5 };
      var expectedNumbers = new List<int>() { 4, 5 };

      // Act
      var res = Miscellaneous.SkipThree(numbers);

      // Assert
      Assert.Equal(expectedNumbers, res);
    }

    [Fact]
    public void FirstTest()
    {
      // Arrange
      var numbers = new List<int>() { 1, 2, 3, 4, 5 };
      var expected = 1;

      // Act
      var res = Miscellaneous.First(numbers);

      // Assert
      Assert.Equal(expected, res);
    }

    [Fact]
    public void FirstNoMatchTest()
    {
      // Arrange
      var numbers = new List<int>() { 1, 2, 3, 4, 5 };

      // Act
      Action act = () => Miscellaneous.First(numbers, n => n > 10);

      // Assert
      Assert.Throws<InvalidOperationException>(act);
    }

    [Fact]
    public void FirstEmptyTest()
    {
      // Arrange
      var numbers = new List<int>();

      // Act
      Action act = () => Miscellaneous.First(numbers);

      // Assert
      Assert.Throws<InvalidOperationException>(act);
    }

    [Fact]
    public void FirstOrDefaultTest()
    {
      // Arrange
      var numbers = new List<int>() { 1, 2, 3, 4, 5 };
      var expectedNumber = 1;

      // Act
      var res = Miscellaneous.FirstOrDefault(numbers);

      // Assert
      Assert.Equal(expectedNumber, res);
    }

    [Fact]
    public void FirstOrDefaultNoMatchTest()
    {
      // Arrange
      var numbers = new List<int>() { 1, 2, 3, 4, 5 };

      // Act
      var res = Miscellaneous.FirstOrDefault(numbers, n => n > 10);

      // Assert
      Assert.True(res == 0);
    }

    [Fact]
    public void FirstOrDefaultEmptyTest()
    {
      // Arrange
      var numbers = new List<int>();

      // Act
      var res = Miscellaneous.FirstOrDefault(numbers, n => n > 10);

      // Assert
      Assert.True(res == 0);
    }

    [Fact]
    public void DistinctTest()
    {
      // Arrange
      var numbers = new List<int>() { 1, 2, 3, 4, 3, 4, 5, 6, 7, 8, 9 };
      var expectedNumbers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

      // Act
      var res = Miscellaneous.Distinct(numbers);

      // Arrange
      Assert.Equal(expectedNumbers, res);
    }
  }
}
