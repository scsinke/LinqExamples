using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqExamples.misc
{
  class Miscellaneous
  {
    public static IEnumerable<T> SkipThree<T>(IEnumerable<T> source)
    {
      return source.Skip(3);
    }

    public static T First<T>(IEnumerable<T> source)
    {
      return source.First();
    }

    public static T First<T>(IEnumerable<T> source, Func<T, bool> predicate)
    {
      return source.First(predicate);
    }

    public static T FirstOrDefault<T>(IEnumerable<T> source)
    {
      return source.FirstOrDefault();
    }

    public static T FirstOrDefault<T>(IEnumerable<T> source, Func<T, bool> predicate)
    {
      return source.FirstOrDefault(predicate);
    }

    public static IEnumerable<T> Distinct<T>(IEnumerable<T> source)
    {
      return source.Distinct();
    }
  }
}
