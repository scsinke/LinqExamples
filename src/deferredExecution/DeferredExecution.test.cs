using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.deferredExecution
{
    public class DeferredExecutionTest
    {
        [Fact]
        public void Test()
        {
            // Arrange
            IList<Trade> trades = new List<Trade>()
            {
                new Trade("TSLA", 366.28),
                new Trade("GOOGL", 1547.23),
                new Trade("AMZN", 3268.61),
                new Trade("RDSA", 12.03),
                new Trade("AIR", 69.58),
                new Trade("FB", 273.72),
                new Trade("MSFT", 211.29),
                new Trade("AAPL", 177.32),
            };

            // Act
            var query = trades.Where(t => t.Price >= 100).Select(t => t.Name);
            
            // Assert

            string[] firstQuery = {"TSLA", "GOOGL", "AMZN", "FB", "MSFT", "AAPL"};
            
            Assert.Equal(firstQuery, query);

            trades.Add(new Trade("NKE", 144.90));
            
            string[] secondQuery = {"TSLA", "GOOGL", "AMZN", "FB", "MSFT", "AAPL", "NKE"};
            
            Assert.Equal(secondQuery, query);
        }
    }
}