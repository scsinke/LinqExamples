using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;

// Opdracht 2 Ordering (order by)
namespace LinqExamples.ordering
{
  class Order
  {
    public IOrderedEnumerable<Student> ByLastname(IEnumerable<Student> students)
    {
      return students.OrderBy(s => s.LastName)
        // .ThenBy(s => s.FirstName)
        ;
    }
  }
}
