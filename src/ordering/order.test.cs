using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.ordering
{
  public class OrderTest
  {
    [Theory]
    [MemberData(nameof(GetStudents))]
    public void OrderedByLastnameTest(List<Student> students)
    {
      // Arrange
      var order = new Order();

      // Act
      var orderedStudents = order.ByLastname(students).ToList();
    
      // Assert
      Assert.Equal("Dicardo", orderedStudents[0].LastName);
      Assert.Equal("Sinke", orderedStudents[1].LastName);
      Assert.Equal("Tusk", orderedStudents[2].LastName);
    }

    public static IEnumerable<object[]> GetStudents()
    {
      yield return new object[]
      {
        new List<Student>() {
          new Student("Carsten", "Sinke", 3),
          new Student("Elon", "Tusk", 4),
          new Student("Daniele", "Dicardo", 5),
        }
      };
    }
  }
}
