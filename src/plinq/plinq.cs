using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;

// In deze query wordt het aantal gewonnen wedstrijden van een speler gegroepeerd per tegenstander. Van deze wedstrijden wordt de gemiddelde speelduur berekend van de gewonnen wedstrijden. Daarnaast worden ook alle wedstrijden teruggegeven.


namespace LinqExamples.plinq
{
  class AtpCalculations
  {
    public IEnumerable<ReturnMatchObject> HeadToHeadSerial(IEnumerable<AtpMatch> matches)
    {
      return
        from m in matches
        group m by new
        {
          winner_name = m.WinnerName,
          loser_name = m.LoserName
        } into mGroup
        select new ReturnMatchObject
        {
          Winner = mGroup.Key.winner_name,
          Loser = mGroup.Key.loser_name,
          Matches = mGroup.OrderBy(m => m.LoserName),
          AveragePlayTime = mGroup.Average(m => m.Minutes)
        };
    }

    public IEnumerable<ReturnMatchObject> HeadToHeadParallel(IEnumerable<AtpMatch> matches)
    {
      // HIGHLIGHT query expression
      return
        from m in matches
          .AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism)
        group m by new
        {
          winner_name = m.WinnerName,
          loser_name = m.LoserName
        } into mGroup
        select new ReturnMatchObject
        {
          Winner = mGroup.Key.winner_name,
          Loser = mGroup.Key.loser_name,
          Matches = mGroup.OrderBy(m => m.LoserName),
          AveragePlayTime = mGroup.Average(m => m.Minutes)
        };
    }
// HIGHLIGHT method based query
//   public IEnumerable<ReturnMatchObject> headToHeadTest(IEnumerable<AtpMatch> matches)
//   {
//     return
//       matches
//       .AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism)
//       .GroupBy(m => new { m.winner_name, m.loser_name })
//       .Select(mGroup => new ReturnMatchObject {
//         Winner = mGroup.Key.winner_name,
//         Loser = mGroup.Key.loser_name,
//         Matches = mGroup.OrderBy(m => m.loser_name),
//         AveragePlayTime = mGroup.Average(m => m.minutes)
//       });
//   }
  }

  class ReturnMatchObject
  {
    public string Winner { get; set; }
    public string Loser { get; set; }
    public IOrderedEnumerable<AtpMatch> Matches { get; set; }
    public double AveragePlayTime { get; set; }
  }
}
