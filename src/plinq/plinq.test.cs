using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.plinq
{
  [Collection(nameof(NotThreadSafeResourceCollection))]
  public class AtpCalculationsTest
  {
    [Fact]
    public void HeadToHeadTest()
    {
      // Deze data komt van een publieke dataset af. https://github.com/JeffSackmann/tennis_atp
      // Further this test will only run locally, the gitlab runners do not have to required specification to run this test in parallel
      string[] files = Directory.GetFiles("../../../src/plinq/atp/", "*.csv");

      var totalMatches = new List<AtpMatch>();

      foreach (var file in files)
      {
        IList<AtpMatch> matches = File
          .ReadAllLines(file)
          .Skip(1)
          .Select(m => AtpMatch.FromCsv(m))
          .ToList();

        totalMatches.AddRange(matches);
      }

      var calc = new AtpCalculations();

      var sq = calc.HeadToHeadSerial(totalMatches);
      var pq = calc.HeadToHeadParallel(totalMatches);

      var sSw = Stopwatch.StartNew();
      foreach (var unused in sq) { }
      sSw.Stop();

      var pSw = Stopwatch.StartNew();
      foreach (var unused in pq) { }
      pSw.Stop();

      Assert.True(sSw.ElapsedMilliseconds > pSw.ElapsedMilliseconds, $"Serial: {sSw.ElapsedMilliseconds} was faster than Parallel: {pSw.ElapsedMilliseconds}");
    }
  }
}
