using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.filtering
{
  public class FilterTests
  {
    [Theory]
    [MemberData(nameof(GetStudents))]
    public void MoreThanYearsTest(List<Student> students, int numberOfExpected)
    {
      // Arrange
      int moreThenYears = 3;
      var filter = new Filter();

      // Act
      var res = filter.WhereMoreThanYears(students, moreThenYears);

      // Assert
      Assert.Equal(numberOfExpected, res.Count());
    }

    public static IEnumerable<object[]> GetStudents()
    {
      yield return new object[]
      {
        new List<Student>() {
          new Student("Carsten", "Sinke", 3),
          new Student("Elon", "Tusk", 4),
          new Student("Daniele", "Dicardo", 5)
        },
        2
      };
      yield return new object[]
      {
        new List<Student>() {
          new Student("Carsten", "Sinke", 4),
          new Student("Elon", "Tusk", 4),
          new Student("Daniele", "Dicardo", 5)
        },
        3
      };
    }
  }
}
