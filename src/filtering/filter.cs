using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;

// Opdracht 1 Filtering (where)
namespace LinqExamples.filtering
{
    class Filter
    {
        public IEnumerable<Student> WhereMoreThanYears(IEnumerable<Student> students, int years)
        {

            // HIGHLIGHT method based query
            // return students.Where(s => s.YearsAtSchool > years);

            // HIGHLIGHT query expression
            return
                from student in students
                where student.YearsAtSchool > years
                select student;
        }
    }
}
