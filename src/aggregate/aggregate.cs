using System.Collections.Generic;
using System.Linq;
using LinqExamples.models;

// Opdracht 6 Aggregates (count, min, max, sum, average)

namespace LinqExamples.aggregate
{
  class Aggregate
  {
    public static int CountTrades(List<Trade> trades)
    {
      return trades.Count();
    }

    public static double MinTradePrice(List<Trade> trades)
    {
      return trades.Min(t => t.Price);
    }

    public static double MaxtradePrice(List<Trade> trades)
    {
      return trades.Max(t => t.Price);
    }

    public static double SumTradePrices(List<Trade> trades)
    {
      return trades.Sum(t => t.Price);
    }
  
    public static double AverageTradePrice(List<Trade> trades)
    {
      return trades.Average(t => t.Price);
    }
  }
}
