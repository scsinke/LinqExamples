using System;
using System.Collections.Generic;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.aggregate
{
  public class AggregateTest
  {

    [Theory]
    [MemberData(nameof(GetTrades))]
    public void CountTest(List<Trade> trades)
    {
      var totalTrade = Aggregate.CountTrades(trades);
      Assert.Equal(11, totalTrade);
    }

    [Theory]
    [MemberData(nameof(GetTrades))]
    public void MinTest(List<Trade> trades)
    {
      var minimumTradePrice = Aggregate.MinTradePrice(trades);
      Assert.Equal(12.03, minimumTradePrice);
    }

    [Theory]
    [MemberData(nameof(GetTrades))]
    public void MaxTest(List<Trade> trades)
    {
      var maxTradePrice = Aggregate.MaxtradePrice(trades);
      Assert.Equal(3268.61, maxTradePrice);

    }
    [Theory]
    [MemberData(nameof(GetTrades))]
    public void SumTest(List<Trade> trades)
    {
      var totalTradePrice = Aggregate.SumTradePrices(trades);
      Assert.Equal(6713, Math.Round(totalTradePrice));
    }
    [Theory]
    [MemberData(nameof(GetTrades))]
    public void AverageTest(List<Trade> trades)
    {
      var averageTradePrice = Aggregate.AverageTradePrice(trades);
      Assert.Equal(610, Math.Round(averageTradePrice));
    }

    public static IEnumerable<object[]> GetTrades()
    {
      yield return new object[]
      {
        new List<Trade>() {
          new Trade("TSLA", 366.28),
          new Trade("GOOGL", 1547.23),
          new Trade("AMZN", 3268.61),
          new Trade("RDSA", 12.03),
          new Trade("AIR", 69.58),
          new Trade("FB", 273.72),
          new Trade("MSFT", 211.29),
          new Trade("AAPL", 177.32),
          new Trade("NKE", 144.90),
          new Trade("DIS", 133.36),
          new Trade("NVDA", 508.60),
        }
      };
    }
  }
}
