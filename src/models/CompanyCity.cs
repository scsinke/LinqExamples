namespace LinqExamples.models
{
  public class Company
  {
    public string Name { get; }

    public int CityId { get; }

    public Company(string name, int cityId)
    {
      Name = name;
      CityId = cityId;
    }
  }

  public class City
  {
    public string Name { get; }
    public int Id { get; }

    public City(string name, int id)
    {
      Name = name;
      Id = id;
    }
  }
}