namespace LinqExamples.models
{
  class AtpMatch
  {
    public string TourneyId { get; set; }
    public string TourneyName { get; set; }
    public string Surface { get; set; }
    public string DrawSize { get; set; }
    public string TourneyLevel { get; set; }
    public string TourneyDate { get; set; }
    public string MatchNum { get; set; }
    public string WinnerId { get; set; }
    public string WinnerSeed { get; set; }
    public string WinnerEntry { get; set; }
    public string WinnerName { get; set; }
    public string WinnerHand { get; set; }
    public string WinnerHt { get; set; }
    public string WinnerIoc { get; set; }
    public string WinnerAge { get; set; }
    public string LoserId { get; set; }
    public string LoserSeed { get; set; }
    public string LoserEntry { get; set; }
    public string LoserName { get; set; }
    public string LoserHand { get; set; }
    public string LoserHt { get; set; }
    public string LoserIoc { get; set; }
    public string LoserAge { get; set; }
    public string Score { get; set; }
    public string BestOf { get; set; }
    public string Round { get; set; }
    public int Minutes { get; set; }
    public string WAce { get; set; }
    public string WDf { get; set; }
    public string WSvpt { get; set; }
    public string W1StIn { get; set; }
    public string W1StWon { get; set; }
    public string W2NdWon { get; set; }
    public string WSvGms { get; set; }
    public string WBpSaved { get; set; }
    public string WBpFaced { get; set; }
    public string LAce { get; set; }
    public string LDf { get; set; }
    public string LSvpt { get; set; }
    public string L1StIn { get; set; }
    public string L1StWon { get; set; }
    public string L2NdWon { get; set; }
    public string LSvGms { get; set; }
    public string LBpSaved { get; set; }
    public string LBpFaced { get; set; }
    public string WinnerRank { get; set; }
    public string WinnerRankPoints { get; set; }
    public string LoserRank { get; set; }
    public string LoserRankPoints { get; set; }

    public static AtpMatch FromCsv(string csvLine)
    {
      string[] values = csvLine.Split(',');
      AtpMatch match = new AtpMatch();

      match.TourneyId = values[0];
      match.TourneyName = values[1];
      match.Surface = values[2];
      match.DrawSize = values[3];
      match.TourneyLevel = values[4];
      match.TourneyDate = values[5];
      match.MatchNum = values[6];
      match.WinnerId = values[7];
      match.WinnerSeed = values[8];
      match.WinnerEntry = values[9];
      match.WinnerName = values[10];
      match.WinnerHand = values[11];
      match.WinnerHt = values[12];
      match.WinnerIoc = values[13];
      match.WinnerAge = values[14];
      match.LoserId = values[15];
      match.LoserSeed = values[16];
      match.LoserEntry = values[17];
      match.LoserName = values[18];
      match.LoserHand = values[19];
      match.LoserHt = values[20];
      match.LoserIoc = values[21];
      match.LoserAge = values[22];
      match.Score = values[23];
      match.BestOf = values[24];
      match.Round = values[25];
      try
      {
        match.Minutes = int.Parse(values[26]);  
      }
      catch (System.Exception)
      {
        match.Minutes = 0;
      }
      match.WAce = values[27];
      match.WDf = values[28];
      match.WSvpt = values[29];
      match.W1StIn = values[30];
      match.W1StWon = values[31];
      match.W2NdWon = values[32];
      match.WSvGms = values[33];
      match.WBpSaved = values[34];
      match.WBpFaced = values[35];
      match.LAce = values[36];
      match.LDf = values[37];
      match.LSvpt = values[38];
      match.L1StIn = values[39];
      match.L1StWon = values[40];
      match.L2NdWon = values[41];
      match.LSvGms = values[42];
      match.LBpSaved = values[43];
      match.LBpFaced = values[44];
      match.WinnerRank = values[45];
      match.WinnerRankPoints = values[46];
      match.LoserRank = values[47];
      match.LoserRankPoints = values[48];

      return match;
    }
  }
}
