namespace LinqExamples.models
{
  public class Trade
  {
    public string Name { get; }
    public double Price { get; }

    public Trade(string name, double price)
    {
      Name = name;
      Price = price;
    }
  }
}
