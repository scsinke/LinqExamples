using System.Collections.Generic;

namespace LinqExamples.models
{
  public class Student
  {
    public string FirstName { get; }
    public string LastName { get; }
    public int YearsAtSchool { get; }

    public List<SchoolClass> Classes { get; }


    public Student(string firstName, string lastName, int yearsAtSchool, List<SchoolClass> classes = null)
    {
      FirstName = firstName;
      LastName = lastName;
      YearsAtSchool = yearsAtSchool;
      Classes = classes;
    }
  }

  public class SchoolClass
  {
    public string Name { get; }
    public List<int> Marks { get; }

    public SchoolClass(string name, List<int> marks = null)
    {
      Name = name;
      Marks = marks ?? new List<int>();
    }
  }
}