using System.Linq;
using LinqExamples.models;
using Xunit;

namespace LinqExamples.linqExtensions
{
  public class ExtensionTest
  {
    [Fact]
    public void WhereTest()
    {
      // Arrange
      var items = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
      var expected = new[] { 2, 4, 6, 8, 10 };

      // Act
      var evenItems = items.NewWhere(x => x % 2 == 0);

      // Assert
      Assert.Equal(expected, evenItems);
    }

    [Fact]
    public void SelectTest()
    {
      // Arrange
      var students = new[]
      {
        new Student("Carsten", "Sinke", 3)
      };
      var expected = new[] { "Carsten" };

      // Act
      var selectedStudents = students.Select(s => s.FirstName);

      // Assert
      Assert.Equal(expected, selectedStudents);
    }

    [Fact]
    public void AnyWithValuesTest()
    {
      var items = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
      var hasElement = items.NewAny();

      Assert.True(hasElement);
    }

    [Fact]
    public void AnyWithoutValuesTest()
    {
      var items = new int[] { };
      var hasElement = items.NewAny();

      Assert.False(hasElement);
    }
  }
}
