using System;
using System.Collections.Generic;

namespace LinqExamples.linqExtensions
{
  public static class EnumerableExtension
  {
    // Without yield performance benefits met yield
    // Bij uitprinten is zonder yield sneller
    // totaal evenveel werk
    // Voor de user is het fijner. De user kan al gaan lezen de data komt later
    public static IEnumerable<T> NewWhere<T>(this IEnumerable<T> items, Func<T, bool> predicate)
    {
      foreach (var item in items)
      {
        if (predicate(item))
        {
          yield return item;
        }
      }
    }

    // HIGHLIGHT NewWhere get values using the iterator
    // public static IEnumerable<T> NewWhere<T>(this IEnumerable<T> items, Func<T, bool> predicate)
    // {
    //   var enumerator = items.GetEnumerator();

    //   while(enumerator.MoveNext())
    //   {
    //     if(predicate(enumerator.Current))
    //     {
    //       yield return enumerator.Current;
    //     }
    //   }
    // }

    public static IEnumerable<TResult> NewSelect<T, TResult>(this IEnumerable<T> items, Func<T, TResult> selector)
    {
      foreach (var item in items)
      {
        yield return selector(item);
      }
    }

    public static IEnumerable<TResult> NewSelectMany<T, TResult>(this IEnumerable<T> items, Func<T, IEnumerable<TResult>> selector)
    {
      foreach (var item in items)
      {
        foreach (var innerItem in selector(item))
        {
          yield return innerItem;
        }
      }
    }

    public static bool NewAny<T>(this IEnumerable<T> items)
    {
      if (items != null && items.GetEnumerator().MoveNext())
        return true;

      return false;
    }
    public static bool NewAny<T>(this IEnumerable<T> items, Func<T, bool> predicate)
    {
      foreach (var item in items)
      {
        if (predicate(item))
          return true;
      }

      return false;
    }

// HIGHLIGHT Own extension to do a deep distinct on objects
    public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
    {
      HashSet<TKey> seenKeys = new HashSet<TKey>();
      foreach (TSource element in source)
      {
        if (seenKeys.Add(keySelector(element)))
        {
          yield return element;
        }
      }
    }
  }
}
