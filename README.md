# LinqExamples

Dit is een demo project om de werking van linq te begrijpen. Het project is opgedeeld uit meerdere folders voor de aan te tonen criterias. Omdat er gevraagd is om zowel `query expression` als `method based queries` zijn ,waar meerdere queries beschikbaar, gemarkeerd met `HIGHLIGHT` in de comments. Hierdonder wordt sequence vs collections besproken en deferred execution.

## Sequence vs Collection

Het verschil tussen een `sequence` en `collection` is in de basis niet veel. Beide zijn een lijst van items. Echter krijg je bij een `collection` meer mogelijkheden om op deze lijst uit te voeren. Denk hierbij als voorbeeld aan de `lengte` van de lijst. Deze lengte kan alleen opgevraagd worden omdat het programma door de hele lijst loopt om te kijken hoeveel items er zijn. Hierdoor wordt elk item in het geheugen geplaats. Als dit gebeurd bij een zeer grote lijst dan kan het geheugen helemaal vol komen te staan. Hierdoor kan het handig zijn om alleen gebruik te maken van een `sequence`. Deze `sequence` kan dan één voor één de items aanbieden, wanneer het item nodig is(`lazy loading`), waardoor er per keer maar één item in het geheugen hoeft te staan. `Linq` kan voordat een item wordt aangeboden eerst bekijken of deze voldoet aan bepaald `predicate`(`lazy evaluation`). Een voorbeeld hiervan is de `Where` clause. Het besparen van geheugen kan alleen als de items nog niet in het geheugen staan. Dit kan bijvoorbeeld door een implementatie met een externe bron zoals een database.

## Deferred Execution

`Deferred Execution` houdt in dat de query pas uitgevoerd wordt wanneer deze nodig is. Door het uitstellen van de query is ook altijd de meest recente data beschikbaar. Als tussendoor de data wijzigd kan dit gewoon mee worden genomen in de query. Echter moet `deferred execution` niet verward worden met `lazy loading`. Bij `lazy loading` wordt de data wanneer nodig aangeboden, maar is het al wel verwerkt. Zie [Sequence vs Collection](##Sequence-vs-Collection)

### example

Onderstaande code staat ook in een bijgeleverde test. Zie het mapje `deferredExecution` onder src.

```cs
IList<Trade> trades = new List<Trade>()
{
  new Trade("TSLA", 366.28),
  new Trade("GOOGL", 1547.23),
  new Trade("AMZN", 3268.61),
  new Trade("RDSA", 12.03),
  new Trade("AIR", 69.58),
  new Trade("FB", 273.72),
  new Trade("MSFT", 211.29),
  new Trade("AAPL", 177.32),
};

var query = from t in trades
            where t.Price >= 100
            select t;

foreach (var trade in query)
{
 System.Console.WriteLine(trade.Name);
}
// Returns:
// TSLA
// GOOGL
// AMZN
// FB
// MSFT
// AAPL

trades.Add(new Trade("NKE", 144.90));

foreach (var trade in query)
{
    System.Console.WriteLine(trade.Name);
}
// Returns:
// TSLA
// GOOGL
// AMZN
// FB
// MSFT
// AAPL
// NKE
```
